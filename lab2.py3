import math
import random

A = 4
B = 7
delta = B-A

def functionUnimodal(arg):
    return arg*arg*math.sin(arg)-2

def functionMultimodal(arg):
    return (arg*arg*math.sin(arg)-2)*math.sin(5*arg)

def randomValue():
    return A+delta*random.random()

def unimodal():
    print('UNIMODAL')
    print(
        '+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')
    print(
        '|  q/P   |   0.9    |   0.91   |   0.92   |   0.93   |   0.94   |   0.95   |   0.96   |   0.97   |   0.98   |   0.99   |')
    print(
        '+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')
    for i in range(1, 21): # q
        print('|', '{:.3f}'.format(i / 200), end="  |")
        epsProbability = (B-A)*i/200
        for j in range(10):
            probability = 0.9+j/100
            N = math.log(1 - probability) / math.log(1 - epsProbability) # N = ln(1 - P) / ln(1 - Pe)
            randomX1 = randomValue()
            for iterator in range(int(N)+1):
                randomX2 = randomValue()
                if functionUnimodal(randomX1) > functionUnimodal(randomX2):
                    randomX1 = randomX2
            if functionUnimodal(randomX1) < -10.0:
                print(' ', '{:.4f}'.format(functionUnimodal(randomX1)), ' |', sep = "", end = "")
            else:
                print('  ', '{:.4f}'.format(functionUnimodal(randomX1)), ' |', sep="", end="")
        print(
            '\n+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')

def multimodal():
    print('MULTIMODAL')
    print(
        '+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')
    print(
        '|  q/P   |   0.9    |   0.91   |   0.92   |   0.93   |   0.94   |   0.95   |   0.96   |   0.97   |   0.98   |   0.99   |')
    print(
        '+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')
    for i in range(1, 21): # q
        print('|', '{:.3f}'.format(i / 200), end="  |")
        epsProbability = (B-A)*i/200
        for j in range(0, 10):
            probability = 0.9+j/100
            N = math.log(1 - probability) / math.log(1 - epsProbability) # N = ln(1 - P) / ln(1 - Pe)
            N = 3*N
            randomX1 = randomValue()
            for iterator in range(int(N)+1):
                randomX2 = randomValue()
                if functionMultimodal(randomX1) > functionMultimodal(randomX2):
                    randomX1 = randomX2
            if functionMultimodal(randomX1) < -10.0:
                print(' ', '{:.4f}'.format(functionMultimodal(randomX1)), ' |', sep = "", end = "")
            else:
                print('  ', '{:.4f}'.format(functionMultimodal(randomX1)), ' |', sep="", end="")
        print(
            '\n+--------+----------+----------+----------+----------+----------+----------+----------+----------+----------+----------+')

def main():
    unimodal()
    multimodal()

if __name__ == '__main__':
    main()