import math
import random
import array

A = -2.0
B = 2.0
delta = B - A
mutationDelta = 0.1

def function(x, y):
    return math.exp(-math.pow(x, 2.))*math.exp(-math.pow(y, 2.))/(1+math.pow(x, 2.)+math.pow(y, 2.))

def randomValue():
    return A+delta*random.random()

def setPrecision():
    for i in range(len(PRECISION)):
        PRECISION[i] = (1. - F[i]/fSum())/3.

def fSum():
    sum =0.

    for i in range(len(F)):
        sum += F[i]

    return sum

def newGeneration(toIgnore):
    Alpha = 0

    for i in range(len(F)):
        if function(X[i], Y[i]) == max(F):
            Alpha = i

    if iteration <=10 or iteration % 10 == 0:
        print('Alpha: ', Alpha)

    newX = [X[Alpha], X[Alpha], 0, 0]
    newY = [0, 0, Y[Alpha], Y[Alpha]]

    flag = Alpha
    for i in range(2, len(F)):
        for j in range(len(F)):
            if j == Alpha:
                continue
            if j != toIgnore:
                if j == flag:
                    continue
                if j != flag:
                    newX[i] = X[j]
                    flag = j
                    break

    flag = Alpha
    for i in range(len(F)-2):
        for j in range(len(F)):
            if j == Alpha:
                continue
            if j != toIgnore:
                if j == flag:
                    continue
                if j != flag:
                    newY[i] = Y[j]
                    flag = j
                    break

    for i in range(len(F)):
        X[i] = newX[i]
        Y[i] = newY[i]

    if iteration <=10 or iteration % 10 == 0:
        print('X: ', X)
        print('Y: ', Y)

    mutation()

def mutation():

    for i in range(len(F)):
        if random.random() < 0.25:
            if random.random() < 0.5:
                X[i] += mutationDelta
            else:
                X[i] -= mutationDelta

    for i in range(len(F)):
        if random.random() < 0.25:
            if random.random() < 0.5:
                Y[i] += mutationDelta
            else:
                Y[i] -= mutationDelta


# Starting values set
X = [0, 0, 0, 0]
Y = [0, 0, 0, 0]

for i in range(len(X)):
    X[i] = randomValue()

for i in range(len(Y)):
    Y[i] = randomValue()

# F(X,Y)
F = [0., 0., 0., 0.]

for i in range(len(F)):
    F[i] = function(X[i], Y[i])

print('X: ', X)
print('Y: ', Y)
print('F: ', F)

# Death precision, look up for setPrecision()
PRECISION = [0., 0., 0., 0.]

# MAIN
MID = 0.

for iteration in range(101):
    if iteration <=10 or iteration % 10 == 0:
        print('__________________________________________________________________________________')
        print('iteration: ', iteration)

    setPrecision()
    keepAlive = random.random()

    if iteration <=10:
        print('P: ', PRECISION)

    if keepAlive < PRECISION[0]:
        newGeneration(0)
    else:
        if keepAlive < PRECISION[0] + PRECISION[1]:
            newGeneration(1)
        else:
            if keepAlive < PRECISION[0] + PRECISION[1] + PRECISION[2]:
                newGeneration(2)
            else:
                newGeneration(3)

    for i in range(len(F)):
        F[i] = function(X[i], Y[i])

    if iteration <=10 or iteration % 10 == 0:
        print('F: ', "\033[32m {}" .format(F))
        print('MAX F: ', max(F))

    MID = 0.

    for i in range(len(F)):
        MID += F[i] / 4

    if iteration <=10 or iteration % 10 == 0:
        print('MID: ', MID)
