import math
import random

A = 4
B = 7
delta = B-A

minT = 0.1
maxT = 10000

def functionUnimodal(arg):
    return arg*arg*math.sin(arg)-2

def functionMultimodal(arg):
    return (arg*arg*math.sin(arg)-2)*math.sin(5*arg)

def randomValue():
    return A+delta*random.random()

def unimodal():
    print('UNIMODAL')
    X1 = randomValue()
    temp = maxT
    print(
        '+-----+--------+-------+-------+---------+-------------+')
    print(
        '| N   | accept | P(Δf) |  x    |  f(x)   | temperature ')
    print(
        '+-----+--------+-------+-------+---------+-------------+')
    N = 0
    X1 = 0
    while temp > minT:
        precision = False
        N +=1
        X2 = randomValue()
        deltafX = functionUnimodal(X2)-functionUnimodal(X1)
        precision = math.exp(-deltafX / temp)
        if precision > 1.0:
            precision = 1.0
        if deltafX <= 0:
            X1 = X2
            accept = True
        else:
            if random.random() <= precision:
                X1 = X2
                accept = True
            else:
                accept = False
        if N == 1:
            precision = 1
        print('|', N, end = " ")
        if N < 100:
            print(' ', end = "")
        if N < 10:
            print(' ', end = "")
        print('|', end = "")
        #print('{:.3f}'.format(temp))
        print(' ', accept, end = " ")
        if accept == True:
            print(' ', end = "")
        print('|', '{:.3f}'.format(precision), '|', '{:.3f}'.format(X1), '|', end ="")
        if functionUnimodal(X1) > 0:
            print(' ', end = "")
        if math.fabs(functionUnimodal(X1)) < 10:
            print(' ', end = "")
        print('{:.4f}'.format(functionUnimodal(X1)), '|', end = "")
        print(' ', '{:.3f}'.format(temp))
        temp = temp*0.95
        print(
            '+-----+--------+-------+-------+---------+-------------+')
    print('RESULT:')
    print('Xmin = ', '{:.3f}'.format(X1))
    print('f(Xmin) = ', '{:.4f}'.format(functionUnimodal(X1)))

def multimodal():
    print('MULTIMODAL')
    X1 = randomValue()
    temp = maxT
    print(
        '+-----+--------+-------+-------+---------+-------------+')
    print(
        '| N   | accept | P(Δf) |  x    |  f(x)   | temperature ')
    print(
        '+-----+--------+-------+-------+---------+-------------+')
    N = 0
    while temp > minT:
        precision = False
        N +=1
        X2 = randomValue()
        deltafX = functionMultimodal(X2)-functionMultimodal(X1)
        precision = math.exp(-deltafX / temp)
        if precision > 1.0:
            precision = 1.0
        if deltafX <= 0:
            X1=X2
            accept = True
        else:
            if random.random() <= precision:
                X1=X2
                accept = True
            else:
                accept = False
        if N == 1:
            precision = 1
        print('|', N, end = " ")
        if N < 100:
            print(' ', end = "")
        if N < 10:
            print(' ', end = "")
        print('|', end = "")
        #print('{:.3f}'.format(temp))
        print(' ', accept, end = " ")
        if accept == True:
            print(' ', end = "")
        print('|', '{:.3f}'.format(precision), '|', '{:.3f}'.format(X1), '|', end ="")
        if functionMultimodal(X1) > 0:
            print(' ', end = "")
        if math.fabs(functionMultimodal(X1)) < 10:
            print(' ', end = "")
        print('{:.4f}'.format(functionMultimodal(X1)), '|', end = "")
        print(' ', '{:.3f}'.format(temp))
        temp = temp*0.95
        print(
            '+-----+--------+-------+-------+---------+-------------+')
    print('RESULT:')
    print('Xmin = ', '{:.3f}'.format(X1))
    print('f(Xmin) = ', '{:.4f}'.format(functionMultimodal(X1)))

def main():
    unimodal()
    print()
    multimodal()

if __name__ == '__main__':
    main()