import numpy

Operators = [ 'mgts        ',
              'rostelecom  ',
              'akado       ',
              'qwerty      ']

A = [[3, 9, 8, 5],
     [2, 6, 7, 8],
     [8, 3, 7, 1],
     [6, 8, 5, 9]]

def M1():
    print("\033[30m {}".format(" "))
    print("#Method 1 (Replacing criteria with constraints)")
    print()

    A = [[3, 9, 8, 5],
         [2, 6, 7, 8],
         [8, 3, 7, 1],
         [6, 8, 5, 9]]

    CritWeightVector = [4., 8., 2., 6.]
    print("Criteria Weight Vector = ", end="")
    print(CritWeightVector)

    # normalisation
    critSum = 0.
    for i in range(len(CritWeightVector)):
        critSum += CritWeightVector[i]

    for i in range(len(CritWeightVector)):
        CritWeightVector[i] = CritWeightVector[i] / critSum

    print()
    print("Criteria Weight Vector (normalized) = ", end="")
    print(CritWeightVector)

    # evaluating alternatives

    #  #1price #2speed #3support #4quality
    # mgts
    # rostelecom
    # akado
    # qwerty

    # actual A - the matrix of criterions
    A = [[3, 9, 8, 5],
         [2, 6, 7, 8],
         [8, 3, 7, 1],
         [6, 8, 5, 9]]

    transA = [[3, 2, 8, 6],
              [9, 6, 3, 8],
              [8, 7, 7, 5],
              [5, 8, 1, 9]]

    print()
    print('            1     2     3     4')

    for i in range(len(A)):
        print(Operators[i], end="")
        for j in range(len(A[i])):
            print('{:.2f}'.format(A[i][j]), sep="  ", end="  ")
        print()

    # quality as main criterion
    minQuality = [8 * 0.2, 9 * 0.5, 8 * 0.3]

    for i in range(len(A)):
        if i != 3:
            minJ = min(transA[i])
            maxJ = max(transA[i])
            for j in range(len(A[i])):
                A[j][i] = (A[j][i] - minJ) / (maxJ - minJ)

    print('\r\nSet quality (4) as main')
    print()
    print('            1     2     3     4')

    for i in range(len(A)):
        print(Operators[i], end="")
        for j in range(len(A[i])):
            print('{:.2f}'.format(A[i][j]), sep="  ", end="  ")
        print()


def utopiaPoint(criteria):
    criteriaRow = [0., 0., 0., 0.]

    for i in range(len(criteriaRow)):
        criteriaRow[i] = A[i][criteria]

    criteriaRow = sorted(criteriaRow)

    print(criteriaRow)

    return criteriaRow[3]

#  #1price #2speed #3support #4quality
# mgts
# rostelecom
# akado
# qwerty

def M2():
    A = [[3, 9, 8, 5],
         [2, 6, 7, 8],
         [8, 3, 7, 1],
         [6, 8, 5, 9]]

    print("\033[33m {}".format(" "))
    print("#Method 2 (Pareto method)")
    print()
    print('            1     2     3     4')

    for i in range(len(A)):
        print(Operators[i], end="")
        for j in range(len(A[i])):
            print('{:.2f}'.format(A[i][j]), sep="  ", end="  ")
        print()

    # utopia criteria 2, 4 speed and quality

    print()
    print('Utopia criteria 2, 4 speed and quality')
    print()

    criteria = [2 - 1, 4 - 1]
    utopiaPoints = [0., 0.]

    print('Criteria rows')
    print()

    for i in range(len(utopiaPoints)):
        utopiaPoints[i] = utopiaPoint(i)

    print()

    print('Manhattan distance')
    print()

    manhattanDistance = [0., 0., 0., 0.]

    for i in range(len(A)):
        print(Operators[i], end="")
        manhattanDistance[i] = utopiaPoints[0] + utopiaPoints[1] - A[i][criteria[0]] - A[i][criteria[1]]
        print(manhattanDistance[i])

    print()

    for i in range(len(manhattanDistance)):
        if manhattanDistance[i] == min(manhattanDistance):
            print('Best alter is ', Operators[i])

def M3():
    print("\033[34m {}".format(" "))
    print("#Method 3 (Weighting and combining criteria)")
    print()

    A = [[3, 9, 8, 5],
         [2, 6, 7, 8],
         [8, 3, 7, 1],
         [6, 8, 5, 9]]

    print('            1     2     3     4')

    for i in range(len(A)):
        print(Operators[i], end="")
        for j in range(len(A[i])):
            print('{:.2f}'.format(A[i][j]), sep="  ", end="  ")
        print()

    print()
    print()
    normalizer = [0., 0., 0., 0.]

    for i in range(len(A)):
        for j in range(len(A)):
            normalizer[i] += A[j][i]

    print('sum A[i] = ', normalizer, sep="")

    for i in range(len(A)):
        for j in range(len(A)):
            A[i][j] = A[i][j] / normalizer[j]

    print()
    print('Normalised')
    print()
    print('            1     2     3     4')

    for i in range(len(A)):
        print(Operators[i], end="")
        for j in range(len(A[i])):
            print('{:.2f}'.format(A[i][j]), sep="  ", end="  ")
        print()

    CritWeightVector = [4., 8., 2., 6.]

    print()
    print("Criteria Weight Vector = ", end="")
    print(CritWeightVector)
    print()

    criteriaVector = numpy.array([0., 0., 0., 0.])

    for i in range(len(criteriaVector)):
        for j in range(len(CritWeightVector)):
            if i != j:
                if CritWeightVector[i] > CritWeightVector[j]:
                    criteriaVector[i] += 1
                if CritWeightVector[i] == CritWeightVector[j]:
                    criteriaVector[i] += 0.5

    print("Criteria Weight Vector pair comparing = ", end="")
    print(criteriaVector)
    print()

    critSum = 0.
    for i in range(len(CritWeightVector)):
        critSum += CritWeightVector[i]

    for i in range(len(CritWeightVector)):
        CritWeightVector[i] = CritWeightVector[i] / critSum

    print("Criteria Weight Vector (normalized) = ", end="")
    print(CritWeightVector)

    critSum = 0.
    for i in range(len(criteriaVector)):
        critSum += criteriaVector[i]

    for i in range(len(criteriaVector)):
        criteriaVector[i] = criteriaVector[i] / critSum

    print()
    print("Criteria Weight Vector pair comparing (normalized) = ", end="")
    print('[', end="")

    for i in range(len(criteriaVector)):
        print('{:.2f}'.format(criteriaVector[i]), sep="", end="")
        if i != 3:
            print(', ', sep="", end="")

    print(']')
    print()

    A = numpy.array([[3, 9, 8, 5],
                     [2, 6, 7, 8],
                     [8, 3, 7, 1],
                     [6, 8, 5, 9]])

    combinedCriteria = numpy.array([0., 0., 0., 0.])
    combinedCriteria = A.dot(criteriaVector)

    print('Combined criteria = [', end="")
    for i in range(len(combinedCriteria)):
        print('{:.2f}'.format(combinedCriteria[i]), end="")
        if i != 3:
            print(', ', sep="", end="")
    print(']')

def M4():
    print("\033[35m {}".format(" "))
    print("#Method 4 (analysis of hierarchies)")

    A = [[3, 9, 8, 5],
         [2, 6, 7, 8],
         [8, 3, 7, 1],
         [6, 8, 5, 9]]

    pairedComparingA = [[0., 0., 0., 0.],
                        [0., 0., 0., 0.],
                        [0., 0., 0., 0.],
                        [0., 0., 0., 0.]]

    matrixSum = numpy.array([[0., 0., 0., 0.],
                             [0., 0., 0., 0.],
                             [0., 0., 0., 0.],
                             [0., 0., 0., 0.]])

    Anew = numpy.array([[0., 0., 0., 0.],
                        [0., 0., 0., 0.],
                        [0., 0., 0., 0.],
                        [0., 0., 0., 0.]])

    for i in range(4):
        print("\r\nPaired comparing matrix by criteria: ", end="")
        print(i + 1)
        print()
        for j in range(4):
            for k in range(4):
                if A[j][i] == A[k][i]:
                    pairedComparingA[j][k] = 1
                if A[j][i] > A[k][i]:
                    if A[j][i] - A[k][i] == 1:
                        pairedComparingA[j][k] = 3
                    if A[j][i] - A[k][i] == 2:
                        pairedComparingA[j][k] = 5
                    if A[j][i] - A[k][i] == 3:
                        pairedComparingA[j][k] = 7
                    if A[j][i] - A[k][i] == 4:
                        pairedComparingA[j][k] = 9
                if A[j][i] < A[k][i]:
                    if A[k][i] - A[j][i] == 1:
                        pairedComparingA[j][k] = 1. / 3
                    if A[k][i] - A[j][i] == 2:
                        pairedComparingA[j][k] = 1. / 5
                    if A[k][i] - A[j][i] == 3:
                        pairedComparingA[j][k] = 1. / 7
                    if A[k][i] - A[j][i] == 4:
                        pairedComparingA[j][k] = 1. / 9

        print('            1     2     3     4')

        for j in range(len(pairedComparingA)):
            print(Operators[j], end="")
            for k in range(len(pairedComparingA[j])):
                print('{:.2f}'.format(pairedComparingA[j][k]), sep="  ", end="  ")
            print()

        for j in range(4):
            for k in range(4):
                matrixSum[i][j] += pairedComparingA[j][k]

        print()
        print('Operator criterias sum = [', end="")

        for j in range(4):
            print('{:.2f}'.format(matrixSum[i][j]), end="")
            if j != 3:
                print(', ', sep="", end="")
        print(']')

        print()
        print("Operator criterias sum (normalised) = [", end="")

        matrixSumSum = 0

        for j in range(4):
            matrixSumSum += matrixSum[i][j]

        for j in range(4):
            matrixSum[i][j] = matrixSum[i][j] / matrixSumSum
            print('{:.2f}'.format(matrixSum[i][j]), end="")
            if j != 3:
                print(', ', end="")
        print(']', end="")
        print()

    print()
    print("Criteria priority:")
    print()

    matrixSum = [0., 0., 0., 0.]
    CritWeightVector = [4., 8., 2., 6.]

    for i in range(4):
        for j in range(4):
            if CritWeightVector[i] == CritWeightVector[j]:
                Anew[j][i] = 1
            if CritWeightVector[i] > CritWeightVector[j]:
                if CritWeightVector[i] - CritWeightVector[j] == 1:
                    Anew[j][i] = 3
                if CritWeightVector[i] - CritWeightVector[j] == 2:
                    Anew[j][i] = 5
                if CritWeightVector[i] - CritWeightVector[j] == 3:
                    Anew[j][i] = 7
                if CritWeightVector[i] - CritWeightVector[j] == 4:
                    Anew[j][i] = 9
            if CritWeightVector[i] < CritWeightVector[j]:
                if CritWeightVector[j] - CritWeightVector[i] == 1:
                    Anew[j][i] = 1. / 3
                if CritWeightVector[j] - CritWeightVector[i] == 2:
                    Anew[j][i] = 1. / 5
                if CritWeightVector[j] - CritWeightVector[i] == 3:
                    Anew[j][i] = 1. / 7
                if CritWeightVector[j] - CritWeightVector[i] == 4:
                    Anew[j][i] = 1. / 9

    print('Priority criteria evaluation matrix:')
    print()
    print('            1     2     3     4')

    for i in range(4):
        print(Operators[i], end="")
        for j in range(4):
            print('{:.2f}'.format(Anew[i][j]), sep="  ", end="  ")
        print()

    for i in range(4):
        for j in range(4):
            matrixSum[i] += Anew[i][j]

    print()
    print("Priority criteria matrix string sum = ", end="")
    print('[', end="")

    for i in range(4):
        print('{:.2f}'.format(matrixSum[i]), end="")
        if i != 3:
            print(', ', sep="", end="")
    print(']')

    matrixSumSum = 0

    for i in range(4):
        matrixSumSum += matrixSum[i]

    print()
    print("Priority criteria matrix string sum  (normalised) = ", end="")
    print('[', end="")

    for i in range(4):
        matrixSum[i] = matrixSum[i] / matrixSumSum

    for i in range(4):
        print('{:.2f}'.format(matrixSum[i]), end="")
        if i != 3:
            print(', ', sep="", end="")
    print(']')

    matrixSum = numpy.array(matrixSum)
    matrixSum = Anew.dot(matrixSum)

    print()
    print("Altera matrix = ", end="")
    print('[', end="")

    for i in range(4):
        print('{:.2f}'.format(matrixSum[i]), end="")
        if i != 3:
            print(', ', sep="", end="")
    print(']')

def main():
    M1()
    M2()
    M3()
    M4()

if __name__ == '__main__':
    main()